# Finansal yatırım araçları
**Tahvil**: Hükümet ve işletmeler tarafından verilen bir borç türüdür. Yerli-yabancı şirket ve hükümetlerin ödünç para karşılığında satmak üzere çıkardıkları belgelerdir (menkul değerler). Tahviller genellikle anaparayı geri ödemenin yanı sıra kupon adı verilen bir faiz ödemesi sunar.
**Bono**: yatırımcıların devletten veya bir kurumdan borç almaları karşılığında borcun varlığını temin eden belgelerdir
**Yatırım Fonu**: çok sayıda hisse senedi tahvil veya diğer yatırımlara sahip olan bir havuzun bir kısmını satın alabileceğin yatırım türü, büyük bir pazara, sektöre ve bölgeye yatırım yapmanı sağlar
**EuroBond**: online olarak da işlem yapılabilen uluslararası piyasalarda yabancı para cinsinden işlem gören yatırım aracıdır

Sun Dec 25 2022 21:37:30 GMT+0300 (GMT+03:00)
